

[![CAMRA](https://camra-files.s3.eu-west-1.amazonaws.com/emails/camra-roundel.png "CAMRA")](https://camra.org.uk)



## Requirements

- PHP Version 8.0+
- MYSQL 5.7+ 

## Laravel setup
[https://laravel.com/docs/8.x/installation](https://laravel.com/docs/8.x/installation)

## Installation

- Clone the Git Repository
- Install composer dependencies
- Install NPM dependencies
- Setup database and ensure .env is configured correctly
- Run artisan migrate:fresh --seed
