<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Brewery extends Model
{
    use HasFactory;

    public function beers(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Beer::class);
    }
}
