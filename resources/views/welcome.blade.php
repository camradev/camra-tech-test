@extends('layouts.app')

@section('content')
    <div class="flex h-screen">
        <div class="m-auto">
            <div class="text-center">
                <img src="https://camra-files.s3.eu-west-1.amazonaws.com/emails/camra-roundel.png" class="inline-block" alt="" width="75">
            </div>

            <div class="mt-5 p-10 border rounded shadow">
                <h1 class="font-bold text-3xl">Requirements:</h1>

                <p class="text-xl leading-10">Take as long as you like to deliver:</p>

                <div class="rounded-md bg-blue-50 p-4 my-4">
                    <div class="flex">
                        <div class="flex-shrink-0 mt-1">
                            <svg class="h-5 w-5 text-blue-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                <path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z" clip-rule="evenodd" />
                            </svg>
                        </div>
                        <div class="ml-3 flex-1 md:flex md:justify-between">
                            <p class="text-lg text-blue-700">
                                Greg would like to be able to search the <strong>beers</strong> using a simple form. <br/>
                                When Greg is searching, he doesn't always know the <strong>beer name</strong>. <br/>
                                Greg wants to see the <strong>search results</strong>.
                            </p>
                        </div>
                    </div>
                </div>

                <h1 class="font-bold text-3xl mt-6">Guidance:</h1>

                <ul role="list" class="divide-y list-disc divide-gray-200 max-w-xl pl-10 my-5">

                    <li class="py-2 pl-5">
                        <strong>Prioritise</strong> backend code over front-end design
                    </li>
                    <li class="py-2 pl-5">You can make the search as simple or as complicated as you like.</li>
                    <li class="py-2 pl-5">You can modify any project files</li>
                    <li class="py-2 pl-5">You can use any 3rd party open source libraries and free to use services.</li>
                    <li class="py-2 pl-5">There is no time limit but try to demonstrate your understanding of PHP/Laravel</li>
                </ul>
            </div>
        </div>
    </div>
@endsection
