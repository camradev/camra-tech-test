<?php

namespace Database\Factories;

use App\Models\Beer;
use Illuminate\Database\Eloquent\Factories\Factory;

class BeerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Beer::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => "{$this->faker->company} {$this->faker->city} {$this->faker->word} {$this->faker->randomElement($this->styles())}",
            'description' => $this->faker->text(rand(100, 500)),
            'abv' => $this->faker->randomFloat(1, 2, 8),
            'style' => $this->faker->randomKey($this->styles()),
            'pump_clip_url' => $this->faker->imageUrl,
        ];
    }

    private function styles(): array
    {
        return [
            'IPA' => 'IPA',
            'PA' => 'Pale Ale',
            'PO' => 'Porter',
            'S' => 'Stout',
            'B' =>'Bitter',
            'X' =>'Mild',
        ];
    }
}
