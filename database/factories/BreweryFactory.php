<?php

namespace Database\Factories;

use App\Models\Brewery;
use Illuminate\Database\Eloquent\Factories\Factory;

class BreweryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Brewery::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $company = $this->faker->company;
        return [
            'name' => "{$company} - Brewery",
            'description' => $this->faker->realText(rand(200, 500)),
            'company_name' => "{$company} {$this->faker->companySuffix}",
            'address_1' => $this->faker->streetAddress,
            'address_town_city' => $this->faker->city,
            'address_postcode' => $this->faker->postcode,
            'status' => $this->faker->randomElement(['OPEN', 'CLOSED', 'OTHER']),
            'image_url' => $this->faker->imageUrl,
        ];
    }
}
