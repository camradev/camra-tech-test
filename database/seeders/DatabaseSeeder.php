<?php

namespace Database\Seeders;

use App\Models\Beer;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Brewery::factory()
            ->count(500)
            ->has(Beer::factory()->count(rand(1,12)), 'beers')
            ->create();
    }
}
